-- Unique marks for male students

Students = LOAD 'datasets/students.tsv' USING PigStorage( '\t', '-noschema') AS (student_id: Long, name: Chararray, surname: Chararray, gender: Chararray, age: Int);

Grades = LOAD 'datasets/grades.tsv' USING PigStorage( '\t', '-noschema') AS (student_id: Long, course: Chararray, mark: Double );

StudentsMale = FILTER Students BY gender == 'M';
MaleGrades = JOIN StudentsMale BY student_id, Grades BY student_id;

Marks = FOREACH MaleGrades GENERATE mark;

MarksNoRep = DISTINCT Marks;
