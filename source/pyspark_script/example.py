from pyspark import SparkContext
from pyspark import SparkConf
import sys

class MyClass():

	def __init__(self,name,age):
		self.name = name
		self.age = age

	def squared_age(self):
		return self.age**2


filename = sys.argv[1]
output = sys.argv[2]

conf = SparkConf().setAppName("My example").setMaster("local")

sc = SparkContext(conf=conf)

data = sc.textFile( "file://" + filename )
data_proc = data.map( lambda l : l.split("\t") )
data_obj = data_proc.map( lambda l : MyClass(l[1],int(l[4])) )
dataf = data_obj.map( lambda o: (o.name, o.squared_age() ) )

dataf.saveAsTextFile( "file://" + output ) 
